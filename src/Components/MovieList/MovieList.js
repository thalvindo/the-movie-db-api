import React, {useState, useEffect} from 'react';
import { Pagination } from 'antd';
import axios from 'axios';
import { useDispatch, useSelector } from 'react-redux';

import { addMovieList } from '../../Redux/blank';
import MovieCard from '../MovieCard/MovieCard';

const MovieList = () => {
    const [page, setPage] = useState(1);
    const { movieList } = useSelector(state => state.blank);
    const dispatch = useDispatch();

    useEffect(() => {
        const getData = async () => {
            const { data } = await axios.get(`https://api.themoviedb.org/3/movie/upcoming?api_key=2fccde01a371b106b09a241d6d1d5b49&page=${page}`)
            dispatch(addMovieList(data.results));
        };
        getData();
    }, [page])

    return (
        <>
            {
                movieList !== undefined ?
                <div className="movie-card-container">
                    {movieList.map((data) => (
                            <MovieCard data={data}/> 

                    ))}
                </div>
                : <></>
            }
            <Pagination defaultCurrent={1} total={230} showSizeChanger={false} onChange={(e) => setPage(e)}/>
        </>
    );
};

export default MovieList;