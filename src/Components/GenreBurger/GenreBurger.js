import React, {useEffect} from 'react';
import axios from "axios";
import { useDispatch, useSelector } from 'react-redux';
import { Menu } from 'antd';
import { AiOutlineMenu } from "react-icons/ai";

import { addGenreList } from '../../Redux/blank';
import './GenreBurger.css';

const GenreBurger = () => {
    const dispatch = useDispatch();
    const { genreList } = useSelector(state => state.blank);

    useEffect(() => {
        const fetchData = async () => {
            const {data} = await axios.get('https://api.themoviedb.org/3/genre/movie/list?api_key=2fccde01a371b106b09a241d6d1d5b49');
            dispatch(addGenreList(data.genres));
        }
        fetchData();
    }, [])

    return (
        <>
          <Menu mode="horizontal" >
            <Menu.SubMenu key="SubMenu" title=" Genre" className='genre-menu' icon={ <AiOutlineMenu />}>
                {genreList.map((genre, key) => {
                    return (
                        <Menu.Item key={key}>
                            {genre.name}
                        </Menu.Item>
                    );
                })}
            </Menu.SubMenu>
          </Menu>
        </>
    )
};
export default GenreBurger;