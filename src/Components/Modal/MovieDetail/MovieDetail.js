import React from 'react';
import { useSelector } from 'react-redux';

import constants from '../../../Constants/Constants';
import './MovieDetail.css';

const MovieDetail = ({isOpen, data, onClose}) => {
    const { genreList } = useSelector(state => state.blank);

    const renderGenres = () => {
        let arrayGenres = [];
        let stringGenres = '';
        genreList.map((genre) => {
            data.genre_ids.map((value) => {
                if(genre.id === value) {
                    arrayGenres.push(genre.name);
                }
            })
        })
        stringGenres = (arrayGenres.toString());
        stringGenres = stringGenres.replace(/,/g, ', ');

        return (
            <>
                <h4 className="genre-movie">{stringGenres}</h4>
            </>
        )
    }
    
    if(!isOpen) return null;
    return (
        <div className="overlay">
            <div className="modal-container">
                <div className="modal-top">
                    <p className='closeBtn' onClick={() => onClose()}>
                        X
                    </p>
                </div>
                <h3 className="movie-title-modal">{data.title}</h3>
                <img src={constants.baseURL + data.poster_path}/>
                <div className="genre-name">
                    <h3 className="genre">Genre: </h3>
                    {
                        renderGenres()
                    }
                </div>
                <h3>Popularity: {data.popularity}</h3>
                <h3>Vote Count: {data.vote_count}</h3>
                <h3>Vote Average: {data.vote_average}</h3>
                <h3>Release: {data.release_date}</h3>
                <h3>Language: {data.original_language}</h3>
                <div className="movie-overview">
                    {data.overview ? 
                        <h4 className="movie-overview-detail">{data.overview}</h4>
                    :
                        <h4 className="no-overview">no overview for this movie</h4>
                    }
                </div>
            </div>
        </div>
    )
};

export default MovieDetail;