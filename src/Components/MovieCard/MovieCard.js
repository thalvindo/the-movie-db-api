import React, {useState} from 'react';

import MovieDetail from '../Modal/MovieDetail/MovieDetail';
import './MovieCard.css';
import constants from '../../Constants/Constants';

const MovieCard = ({data}) => {
    const [isOpenModal, setIsOpenModal] = useState(false);

    const doSomething = () => {
        setIsOpenModal(true);
    };

    return (
        <div className="movie-card-container">
            <MovieDetail isOpen={isOpenModal} data={data} onClose={() => setIsOpenModal(false)}/>
            <div className="movie-card" onClick={() => doSomething()}>
                <h3 className="movie-title">{data.title}</h3>
                <img src={constants.baseURL + data.poster_path} />
            </div>
        </div>
    )
};

export default MovieCard;