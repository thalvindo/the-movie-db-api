import { configureStore } from '@reduxjs/toolkit'
import blankReducer from './blank';

export const store = configureStore({
  reducer: {
    blank: blankReducer
  },
})