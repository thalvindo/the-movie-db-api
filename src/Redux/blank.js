import { createSlice } from '@reduxjs/toolkit'

export const blankSlice = createSlice({
  name: 'list',
  initialState: {
    genreList: [],
    movieList: []
  },
  reducers: {
    addGenreList: (state, action) => {
      state.genreList = action.payload
    },
    addMovieList: (state, action) => {
      state.movieList = action.payload
    }
  }
})

export const { addGenreList, addMovieList } = blankSlice.actions

export default blankSlice.reducer