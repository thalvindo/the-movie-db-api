import './App.css';
import GenreBurger from './Components/GenreBurger/GenreBurger';
import Title from './Components/Title/Title';
import MovieList from './Components/MovieList/MovieList';

const App = () => {
  return (
    <div className="App">
        <Title />
        <GenreBurger />
        <MovieList />
    </div>
  );
};

export default App;
